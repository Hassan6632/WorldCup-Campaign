<?php include('head.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="hero-area">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="assets/img/1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="assets/img/2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="assets/img/3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<section id="dealling">
    <div class="section-padding-2">
        <div class="container">
            <div class="section-title text-center">
                <h3>Submission deadline May 19, 2018</h3>
            </div>

            <div class="dealing-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                    <img class="card-img-top" src="assets/img/p03.jpg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                <img class="card-img-top" src="assets/img/p02.jpg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                <img class="card-img-top" src="assets/img/p01.jpg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                <img class="card-img-top" src="assets/img/p04.jpg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                <img class="card-img-top" src="assets/img/p05.jpg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-dealing">
                            <div class="card">
                                <a href="details.php">
                                <img class="card-img-top" src="assets/img/p06.jpeg" alt="Card image cap">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                    <hr>
                                    <div class="single-crd-bdy">
                                        <div class="single-crd-bdy-part">
                                            <h6>By Muhtarima Sifat</h6>
                                        </div>
                                        <div class="single-crd-bdy-part">
                                            <button data-toggle="modal" data-target="#exampleModalCenter"><i class="icofont icofont-like"></i>Vote</button>
                                            <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                        </div>
                                        <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary mdl-btn" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary mdl-btn">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/jquery-3.2.1.min.js"></script>

<!--    [Popper Js] -->
<script src="assets/js/popper-1.14.3.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="assets/js/bootstrap.min.js"></script>
<script>
    $('#myModal').on('hidden.bs.modal', function(e) {
        // do something...
    })
    /*$('#myModal').modal('toggle')*/

</script>
<!--    [Finish Section Area]-->
<?php include('footer.php'); ?>
