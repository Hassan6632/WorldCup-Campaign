<?php require('head.php'); ?>
<!--    [ Strat Section Area]-->
<section id="story">
    <div class="section-padding-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="page-title text-center">
                        <h2>Story</h2>
                        <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque!</h6>
                    </div>
                </div>
            </div>
            <div class="story-content">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="siingle-story">
                            <div class="card">
                                <div class="card-body">
                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/12cNBNKIXv8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <h4>By - Shamim Hasan Sarkar</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="siingle-story">
                            <div class="card">
                                <div class="card-body">
                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/0fGqs2Wiewg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <h4>By - Shamim Hasan Sarkar</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="siingle-story">
                            <div class="card">
                                <div class="card-body">
                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/0fGqs2Wiewg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <h4>By - Shamim Hasan Sarkar</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="siingle-story">
                            <div class="card">
                                <div class="card-body">
                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/0fGqs2Wiewg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <h4>By - Shamim Hasan Sarkar</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
