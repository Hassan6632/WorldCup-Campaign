<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Torun WorldCup</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/fev.png">


</head>

<body>

    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div id="header" class="main-menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <a href="index.php" class="logo">
                            <img src="assets/img/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-lg-9">
                        <div class="head-top-menu">
                            <nav class="navbar navbar-expand-lg navbar-light ">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="about.php">About</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="populer.php">Popular</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="stars-story.php">Story</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="admin-submit.php">Submit</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sister-consurn-content">
            <div class="container">
                <div class="row justify-content-end">
                    <div class="col-lg-9">
                        <div class="sister-consurn">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="consurn d-table">
                                        <div class="consurn-content d-table-cell">
                                            <div class="web-title">
                                                <h2>Torun Digital World Cup</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="consurn d-table">
                                        <div class="consurn-content d-table-cell">
                                            <div class="sister-con-logo">
                                                <img class="img-responsive" src="assets/img/sl02.png" alt="">
                                            </div>
                                            <div class="sister-con-logo">
                                                <img class="img-responsive" src="assets/img/sl03.png" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="campaign" class="submition submition-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="campaign-pic">
                            <img src="assets/img/ban02.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="total-submission text-center">
                            <a href="#">Total Submissions: 252</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
