<?php require('head.php'); ?>
<!--    [ Strat Section Area]-->
<section id="submission">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="page-title text-center">
                        <h2>Attent Torun Digital World Cup Contest</h2>
                        <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque!</h6>
                    </div>
                </div>
            </div>
            <form action="">
                <div class="submission-form">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="single-input text-center">
                                <div class="up-img-here">
                                    <img id="test" src="" alt="">
                                </div>
                                <div class="upload-btn-wrapper text-center">
                                    <button class="btn">Upload Image <i class="icofont icofont-image"></i></button>
                                    <input id="tstpic" name="photo" type="file" />
                                </div>
                            </div>
                            <div class="single-input">
                                <label for="">Name</label>
                                <input type="text" placeholder="Your Name">
                            </div>
                            <div class="single-input">
                                <label for="">Select Your Team</label>
                                <input list="browsers">
                                <datalist id="browsers">
                                    <option value="Internet Explorer"></option>
                                    <option value="Firefox"></option>
                                    <option value="Chrome"></option>
                                    <option value="Opera"></option>
                                    <option value="Safari"></option>
                                </datalist>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="single-input">
                                        <label for="">Phone</label>
                                        <input type="text" placeholder="Phone">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="single-input">
                                        <label for="">Email</label>
                                        <input type="text" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="single-input">
                                <label for="">TITLE</label>
                                <input type="text" placeholder="Title">
                            </div>
                            <div class="single-input">
                                <label for="">Address</label>
                                <input type="text" placeholder="Address">
                            </div>

                            <div class="single-input">
                                <label for="">Description</label>
                                <textarea type="text" placeholder="Description"></textarea>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="single-input single-input-sub">
                                        <button>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#test').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#tstpic").change(function() {
            readURL(this);
        }

    );

</script>
