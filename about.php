<?php require('head.php'); ?>
<!--    [ Strat Section Area]-->
<section id="about">
    <div class="section-padding-2">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="page-title text-center">
                        <h2 class="text-uppercase">About</h2>
                        <h6>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, itaque!</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<section id="about-description">
    <div class="section-padding-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="description-content">
                        <p>Words fall short for expressing mothers’ sacrifices to fulfill our desires. It seems that moms really can do anything in order to bring smile on the faces of the apples of their eyes. Yes, the sacrifices our mothers make are priceless; but can’t we at least try to fulfill one of her wishes?</p>
                        <p>To mark this year’s Mother’s Day, The Daily Star and CBL Munchee Milkaas have initiated a contest “মা-এর ইচ্ছাপূরণ”.</p>

                        <div class="participate">
                            <h3>How to participate:</h3>
                            <ul class="preti-list">
                                <li>Upload your picture with your mother.</li>
                                <li>Describe your mother’s wish and your plan to make your mother’s wish come true.</li>
                                <li>Share the post on your Facebook timeline and get votes from your friends.</li>
                                <li>Upload your picture with your mother.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="description-content-2">
                        <div class="query-contact">

                            <div class="query-contact-content">
                                <div class="page-title">
                                    <h3>For any campaign query, you can contact-</h3>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <img class="img-responsive" src="assets/img/tajdin-hasan.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <div class="query-contact-part-txt">
                                                        <p>Name: PLAB</p>
                                                        <p>Desig: PROJECT DEVELOPER</p>
                                                        <p>Preneurlab</p>
                                                        <p>Preneurlab@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <img class="img-responsive" src="assets/img/tajdin-hasan.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <div class="query-contact-part-txt">
                                                        <p>Name: PLAB</p>
                                                        <p>Desig: PROJECT DEVELOPER</p>
                                                        <p>Preneurlab</p>
                                                        <p>Preneurlab@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="description-content-3">
                        <div class="price-txt">
                            <img src="assets/img/stiker.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--<section id="contact">
    <div class="section-padding-2">
        <div class="container">
            <div class="description-content">
                <div class="query-contact">
                    <div class="page-title text-center">
                        <h3>For any campaign query, you can contact-</h3>
                    </div>

                    <div class="query-contact-content">
                        <div class="row justify-content-center">
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <img class="img-responsive" src="assets/img/tajdin-hasan.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <div class="query-contact-part-txt">
                                                        <p>Name: PLAB</p>
                                                        <p>Desig: PROJECT DEVELOPER</p>
                                                        <p>Preneurlab</p>
                                                        <p>Preneurlab@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <img class="img-responsive" src="assets/img/tajdin-hasan.jpg" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="query-contact-part">
                                            <div class="query-contact-fream d-table">
                                                <div class="query-contact-container d-table-cell">
                                                    <div class="query-contact-part-txt">
                                                        <p>Name: PLAB</p>
                                                        <p>Desig: PROJECT DEVELOPER</p>
                                                        <p>Preneurlab</p>
                                                        <p>Preneurlab@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--    [Finish Section Area]-->
<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
