<?php require('head.php'); ?>
<!--    [ Strat Section Area]-->
<section id="detail">
    <div class="section-padding-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-dealing">
                        <div class="card">
                            <img class="card-img-top" src="assets/img/p06.jpeg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">My Mother's wish: Travel to sylhe</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eaque nobis quaerat repudiandae. Rerum mollitia explicabo excepturi, reprehenderit, nesciunt quod doloremque vel eveniet repudiandae, assumenda quo! Atque earum aspernatur, explicabo!</p>
                                <hr>
                                <div class="single-crd-bdy">
                                    <div class="single-crd-bdy-part">
                                        <h6>By Muhtarima Sifat</h6>
                                    </div>
                                    <div class="single-crd-bdy-part text-right">
                                        <button><i class="icofont icofont-like"></i>Vote</button>
                                        <button><i class="icofont icofont-social-facebook"></i> Share</button>
                                    </div>
                                    <p><i class="icofont icofont-box"></i><span> Votes: 255</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single-popular">
                        <a href="details.php">
                            <img src="assets/img/p01.jpg" alt="">
                            <h5>Name <span> <i class="icofont icofont-box"></i>1916870 VOTE</span></h5>
                        </a>
                    </div>
                    <div class="single-popular">
                        <a href="details.php">
                            <img src="assets/img/p01.jpg" alt="">
                            <h5>Name <span> <i class="icofont icofont-box"></i>1916870 VOTE</span></h5>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
